const express = require('express')
const router = express.Router()
const auth = require('../auth')
const UserController = require('../controllers/user')

// [SECTION] Primary Routes

router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(result => res.send(result))
})

router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
})

router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result))
})

router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
    UserController.get({ userId: user.id }).then(user => res.send(user))
})
router.post('/addcategory', auth.verify, (req,res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        categoryName: req.body.categoryName,
        categoryType: req.body.categoryType,
    };
    UserController.addCategory(params).then((result) => res.send(result));
});
router.post('/addrecord', auth.verify, (req,res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        categoryName: req.body.categoryName,
        categoryType: req.body.categoryType,
        amount: req.body.amount,
        description: req.body.description
    };
    UserController.addRecord(params).then((result) => res.send(result));
});


// [SECTION] Secondary Routes

router.put('/updatecategory', auth.verify, (req, res) => {
    const params = {
        //categoryId: req.body._id,
        userId: auth.decode(req.headers.authorization).id,
        categoryName: req.body.categoryName,
        categoryType: req.body.categoryType,
    };
    UserController.update(params).then(result => res.send(result))
})

router.put('/change-password', (req, res) => {
    UserController.changePassword()
})

// [SECTION] Integration Routes

router.post('/verify-google-id-token', async (req, res) => {
    res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
    
})

module.exports = router