const bcrypt = require('bcrypt')
const { OAuth2Client } = require('google-auth-library')
const User = require('../models/user')
const auth = require('../auth')
const clientId = '529678454482-kuijd2fp9r549gr2a8kbac9vhqffthdo.apps.googleusercontent.com'

module.exports.emailExists = (params) => {
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10),
		//
        loginType: 'email'
	})

	return user.save().then((user, err) => {
		return (err) ? false : true
	})
}

module.exports.login = (params) => {
	return User.findOne({ email: params.email }).then(user => {
		if (user === null) {
			return { error: 'does-not-exist'}
		}
		if (user.loginType !== 'email'){
			return { error: 'login-type-error'}
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if(isPasswordMatched){
			return { accessToken: auth.createAccessToken(user.toObject())}
		} else {
			return { error: 'incorrect-password'}
		}
	})
}

module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}

module.exports.addCategory = (params) => {
    return User.findById(params.userId).then((user) => {
        user.categories.push({
            categoryName: params.categoryName,
            categoryType: params.categoryType,
        });
        return user.save().then((user, err) => {
            return err ? false : true
        });
    });
}

//add record
module.exports.addRecord =(params) => {
    return User.findById(params.userId).then((user) => {
        user.records.push({
            categoryName: params.categoryName,
            categoryType: params.categoryType,
            amount: params.amount,
            description: params.description
        });
        return user.save().then((user, err) => {
            return err ? false : true
        });
    });
}

//////////update category ///////////////

module.exports.update = (params) => {
	const updates = {
		categoryName: params.categoryName,
        categoryType: params.categoryType
	}
	return User.findByIdAndUpdate(params.userId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}
/////////////////////////////////////
module.exports.updateDetails = (params) => {
	
}

module.exports.changePassword = (params) => {
	
}

module.exports.verifyGoogleTokenId = async (tokenId) => {
	const client = new OAuth2Client(clientId);
	const data = await client.verifyIdToken({ //communicate siya sa database
		idToken: tokenId,
		audience: clientId
	})
	console.log(data.payload);
	if(data.payload.email_verified === true) {
		//gagamitin lang si await if mayn nagrereturn ng promise
		//multiple awaits kahit isang async lang
		const user = await User.findOne({ email: data.payload.email });
		//database ung iniintay niya
		//console.log(user);
		//return true
		if (user !== null) {
			if (user.loginType === 'google'){
				return {accessToken: auth.createAccessToken(user.toObject())}
			} else {
				return { error: "login-type-error"}
			}
		} else {
			let user = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: 'google'
			})
			return user.save().then((user, err) => {
				return {accessToken: auth.createAccessToken(user.toObject())}
			})
		}
	} else {
		return { error: "google-auth-error" }
	}
	return true
};

// mongoose Object
// user{
// 	save: () => {}
// 	findOne
// }
//cinoconvert niya ung mongoose object into plaine javascript object
// user {
// 	firstName:
// 	lastName:
// }
//toObject
//si mongoose