const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')
require('dotenv').config();


app.use(cors());
app.options("*", cors());

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://admin:panutsaILOVEYOU11@wdc028-course-booking.lksmc.mongodb.net/capstonethree-booking?retryWrites=true&w=majority', { 
	useNewUrlParser: true, 
	useUnifiedTopology: true 
})

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const userRoutes = require('./routes/user')


app.use('/api/users', userRoutes)


app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
})